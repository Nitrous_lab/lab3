public class Problem15 {
    public static void main(String[] args) {
        for( int i = 5; i >=0; i = i-1){
            for(int j = 1; j<= i; j = j+1){
                System.out.print(j);
            }
            System.out.println("");
        }
    }
}
/* i    j   
   5    12345     12345
   4    12345     1234
   3    12345     123
   2    12345     12
   1    12345     1
    */
