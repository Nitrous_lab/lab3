public class Problem16 {
    public static void main(String[] args) {
        for( int i = 1; i <=5; i = i+1){
            for(int j = 1; j<= 5; j = j+1){
                if (j < i){
                    System.out.print(" ");
                }
                else{
                    System.out.print(j);
                }
                
            }
            System.out.println("");
        }
    }
}
/* i    j   
   1    12345     12345
   2    12345      2345
   3    12345       345
   4    12345        45
   5    12345         5
    */

