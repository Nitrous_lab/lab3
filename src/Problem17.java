public class Problem17 {
    public static void main(String[] args) {
        for( int i = 5; i >=0; i = i-1){
            for(int j = 1; j<= 5; j = j+1){
                if (j <= i){
                    System.out.print(" ");
                }
                else{
                    System.out.print(j);
                }
                
            }
            System.out.println("");
        }
    }
}
/* i    j   
   5    12345             5
   4    12345            45
   3    12345           345
   2    12345          2345
   1    12345         12345
    */
