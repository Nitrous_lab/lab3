import java.util.Scanner;

public class Problem18 {
    
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        while(true) {
            
            int inputKeyMenu = 0;
            int inputNum = 0;
            System.out.print("Please select star type [1-4,5 is Exit]: ");
            inputKeyMenu = sc.nextInt();
            if (inputKeyMenu == 5)break;
            switch (inputKeyMenu) {
                case 1:
                    System.out.print("Please input number: ");
                    inputNum = sc.nextInt();
                    for( int i = 0; i <inputNum; i = i+1){
                        for(int j = 0; j< i +1; j = j+1){
                            System.out.print("*");
                        }
                        System.out.println("");
                    }
                    break;
                    
            
                case 2:
                    System.out.print("Please input number: ");
                    inputNum = sc.nextInt();
                    for( int i = inputNum; i >0; i = i-1){
                        for(int j = 0; j< i ; j = j+1){
                            System.out.print("*");
                        }
                        System.out.println("");
                    }
                    break;

                    
                case 3:
                    System.out.print("Please input number: ");
                    inputNum = sc.nextInt();
                    for( int i = 0; i <inputNum; i = i+1){
                    
                        for(int j = 0; j< inputNum ; j = j+1){
                            if ( j < i){
                                System.out.print(" ");
                            }
                            else{
                                System.out.print("*");
                            }
                        }
                        System.out.println("");
                    }
                    break;
                    
                case 4:
                    System.out.print("Please input number: ");
                    inputNum = sc.nextInt();
                    for( int i = inputNum; i >=0; i = i-1){
                    
                        for(int j = 0; j< inputNum ; j = j+1){
                            if ( j >= i){
                                System.out.print("*");
                            }
                            else{
                                System.out.print(" ");
                            }
                        }
                        System.out.println("");
                    }
                    break;
                    
                default:
                    System.out.println("Error: Please input number between 1-5");
                    break;
            }
        }
        System.out.println("Bye bye!!!");
        sc.close();

    }
}
