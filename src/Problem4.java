import java.util.Scanner;
public class Problem4 {
    public static void main(String[] args) {
        int num ;
        double avg ;
        int sum = 0;
        int count = 0 ;
        
        Scanner sc = new Scanner(System.in);

        
        while(true) {
            System.out.print("Please input number :");
            num = sc.nextInt();
            if (num == 0) break;
            sum = sum + num;
            count++ ;
            avg = sum / count;
            System.out.println("Sum: "+ sum + " Avg: " + avg);
        }

        System.out.println("Bye");

        sc.close();   
    }
}
