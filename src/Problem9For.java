import java.util.Scanner;

public class Problem9For {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num ;
        System.out.print("Please input n: ");
        num = sc.nextInt();
        for( int i = 1; i <=num; i = i+1){
            for(int j = 1; j <=num; j = j+1){
                System.out.print(j);
            }
            System.out.println("");
        }
        sc.close();
    }
}
